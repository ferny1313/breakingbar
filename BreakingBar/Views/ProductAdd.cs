﻿using BreakingBar.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakingBar.Views
{
    public partial class ProductAdd : Form
    {
        public bool Choosen;
        public Product newProduct = new Product();
        public ProductAdd()
        {
            
            Choosen = false;
            InitializeComponent();
        }

        private void textBoxCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            Models.CustomKeyPress.GetOnlyNumbersAndOneDot(e, textBoxCost.Text);
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {

            Models.CustomKeyPress.ChangeToUpperCase(e, textBoxName.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Choosen = true;
            newProduct.name = textBoxName.Text;
            float cost = 0;
            float.TryParse( textBoxCost.Text,out cost);
            newProduct.cost = cost;

            this.Close();

        }
    }
}
