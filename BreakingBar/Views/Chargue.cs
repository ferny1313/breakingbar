﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakingBar.Views
{
    public partial class Chargue : Form
    {
        public Chargue(float totalDue)
        {
            Choosen = false;
            TotalDue = totalDue;
            InitializeComponent();
        }

        public bool Choosen;
        public float CashChange = 0;
        float TotalDue = 0;

        private void Chargue_Load(object sender, EventArgs e)
        {
            labelTotal.Text = TotalDue.ToString();
            textBoxCash.Text = "0.00";
            textBoxChange.Text = "0.00";

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Models.CustomKeyPress.GetOnlyNumbersAndOneDot(e, (sender as TextBox).Text);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Escape))
            {
                // Alt+F pressed
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            float cash = 0; 
                float.TryParse(textBoxCash.Text,out cash);

            if(cash == TotalDue)
            {
                textBoxChange.BackColor = Color.Green;
                textBoxChange.Text = "0.00";
            }
            else if (cash > TotalDue)
            {
                textBoxChange.BackColor = Color.Gold;
                textBoxChange.Text = (cash - TotalDue).ToString();
            }
            else if (cash < TotalDue)
            {
                textBoxChange.BackColor = Color.Red;
                textBoxChange.Text = (TotalDue - cash).ToString();
            }
        }

        private void buttonPay_Click(object sender, EventArgs e)
        {
            if(float.Parse(textBoxCash.Text) < 1)
            {
                MessageBox.Show("ES NECESARIO INGRESAR LA CANTIDAD A PAGAR");
            }
            else
            {
                if (float.Parse(textBoxCash.Text) < TotalDue)
                {
                    MessageBox.Show("FALTA DINERO!");
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("¿REALIZAR OPERACIÓN?", "¿REALIZAR OPERACIÓN?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        Choosen = true;
                        float.TryParse(textBoxChange.Text, out CashChange);
                        this.Close();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
