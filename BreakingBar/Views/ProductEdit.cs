﻿using BreakingBar.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakingBar.Views
{
    public partial class ProductEdit : Form
    {
        public Product editedItem;
        public bool Choosen;
        public ProductEdit(Product item)
        {
            Choosen = false;
            editedItem = item;
            InitializeComponent();
        }

        private void ProductEdit_Load(object sender, EventArgs e)
        {
            labelProduct.Text = editedItem.name;
            textBoxName.Text = editedItem.name;
            textBoxCost.Text = editedItem.cost.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Choosen = true;
            float cost = 0;
            if (float.TryParse(textBoxCost.Text, out cost))
                editedItem.cost = cost;
            editedItem.name = textBoxName.Text;
     

            this.Close();
        }

        private void textBoxName_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            Models.CustomKeyPress.ChangeToUpperCase(e, textBoxName.Text);
        }

        private void textBoxCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            Models.CustomKeyPress.GetOnlyNumbersAndOneDot(e, textBoxName.Text);

        }
    }
}
