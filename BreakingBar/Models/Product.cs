﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakingBar.Models
{
    public class Product
    {
        public int id { get; set; } = 0;
        public string name { get; set; } = "no name";
        public float cost { get; set; } = 0.0F;


        public List<Product> GetProduct(int id_product)
        {
            List<Product> listAll = new List<Product>();

            string query = "SELECT * FROM product WHERE id = " + id_product;
            try
            {
                using (
                SQLiteConnection connection =
                    new SQLiteConnection(
                        "Data Source=" + Application.StartupPath +
                        "/DB.db;Version=3;New=False;Compress=True;"))
                {
                    connection.Open();

                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    SQLiteDataReader reader = command.ExecuteReader();
                    listAll = new MySqlFunctions().MapReaderToList<Product>(reader);

                    reader.Close();
                    connection.Dispose();
                }
            }
            catch (Exception zz)
            {
                System.Diagnostics.Debugger.Break();
            }


            return listAll;
        }



        public bool Add(Product newItem)
        {
            bool response = false;


            List<Product> listAll = new List<Product>();

            string query = $"INSERT INTO product (name,cost) VALUES ('{newItem.name}','{newItem.cost}')";
            try
            {
                using (
                SQLiteConnection connection =
                    new SQLiteConnection(
                        "Data Source=" + Application.StartupPath +
                        "/DB.db;Version=3;New=False;Compress=True;"))
                {
                    connection.Open();

                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    response = command.ExecuteNonQuery() == 1 ? true : false;
                    connection.Dispose();
                }
            }
            catch (Exception zz)
            {
                System.Diagnostics.Debugger.Break();
            }


            return response;
        }

        public bool Update(Product item)
        {
            bool response = false;


            List<Product> listAll = new List<Product>();

            string query = $"UPDATE product SET name = '{item.name}' , cost = '{item.cost}' WHERE id = {item.id}";
            try
            {
                using (
                SQLiteConnection connection =
                    new SQLiteConnection(
                        "Data Source=" + Application.StartupPath +
                        "/DB.db;Version=3;New=False;Compress=True;"))
                {
                    connection.Open();

                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    response = command.ExecuteNonQuery() == 1 ? true : false;
                    connection.Dispose();
                }
            }
            catch (Exception zz)
            {
                System.Diagnostics.Debugger.Break();
            }


            return response;
        }


        public bool Delete(int id_product)
        {
            bool response = false;


            List<Product> listAll = new List<Product>();

            string query = $"DELETE FROM product WHERE id = {id_product}";
            try
            {
                using (
                SQLiteConnection connection =
                    new SQLiteConnection(
                        "Data Source=" + Application.StartupPath +
                        "/DB.db;Version=3;New=False;Compress=True;"))
                {
                    connection.Open();

                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    response = command.ExecuteNonQuery() == 1 ? true : false;
                    connection.Dispose();
                }
            }
            catch (Exception zz)
            {
                System.Diagnostics.Debugger.Break();
            }


            return response;
        }


    }
}
