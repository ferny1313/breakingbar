﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data;
using System.Windows.Forms;
using SQLite;
using System.Data.SQLite;
using System.ComponentModel;

namespace BreakingBar.Models
{
    class MySqlFunctions
    {
        public BindingList<T> SelectDataFromDB<T>(string WHERE_PROPERTIES)
        {

            List<string> listColumns = new List<string>();
            string query = $"SELECT ";


            //GET COLUMNS NAMES.
            {
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    listColumns.Add(property.Name);
                }
            }

            //SET COLUMNS NAMES ON QUERY
            for (int i = 0; i < listColumns.Count; i++)
            {
                query += listColumns[i];
                if (i != listColumns.Count - 1)
                    query += ",";
            }
            query += $" FROM product ";

            if (!string.IsNullOrWhiteSpace(WHERE_PROPERTIES))
                query += $" WHERE {WHERE_PROPERTIES}";


            BindingList<T> listData = new BindingList<T>();


            try
            {
                using (
                SQLiteConnection connection =
                    new SQLiteConnection(
                        "Data Source=" + Application.StartupPath +
                        "/DB.db;Version=3;New=False;Compress=True;"))
                {
                    connection.Open();

                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    SQLiteDataReader reader = command.ExecuteReader();
                    listData = MapReaderToListBinding<T>(reader);

                    reader.Close();
                    connection.Dispose();
                }
            }
            catch (Exception zz)
            {
                System.Diagnostics.Debugger.Break();
            }

            //MySqlConnection.ClearPool(connection);

            return listData;
        }


        /// <summary>
        /// Get Maping of ReaderData to T class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <returns></returns>
        public List<T> MapReaderToList<T>(IDataReader reader)
        {
            List<T> listData = new List<T>();
            T newObject = default(T);
            while (reader.Read())
            {
                newObject = Activator.CreateInstance<T>();
                foreach (PropertyInfo property in newObject.GetType().GetProperties())
                {
                    if (!Equals(reader[property.Name], DBNull.Value))
                    {
                        property.SetValue(newObject, Convert.ChangeType(reader[property.Name], property.PropertyType), null);
                    }
                }
                listData.Add(newObject);
            }
            return listData;
        }

        private BindingList<T> MapReaderToListBinding<T>(IDataReader reader)
        {
            BindingList<T> listData = new BindingList<T>();
            T newObject = default(T);
            while (reader.Read())
            {
                newObject = Activator.CreateInstance<T>();
                foreach (PropertyInfo property in newObject.GetType().GetProperties())
                {
                    if (!Equals(reader[property.Name], DBNull.Value))
                    {
                        property.SetValue(newObject, Convert.ChangeType(reader[property.Name], property.PropertyType), null);
                    }
                }
                listData.Add(newObject);
            }
            return listData;
        }

    }
}
