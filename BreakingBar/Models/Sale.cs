﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakingBar.Models
{
    class Sale
    {
        public int id_product { get; set; }
        public float quantity { get; set; } = 1;
        public float price { get; set; }
        public DateTime datetime { get; set; }
        public string product_name { get; set; }


        public List<Sale> ToList(string sqlite_query_where_option)
        {
            List<Sale> list = new List<Sale>();

            string query = $" SELECT MIN(sale.id) AS id, id_product, SUM(quantity) AS quantity, SUM(price) AS price, MIN(datetime) AS datetime, MIN(name) as product_name   " +
                $" FROM sale " +
                $" JOIN product ON product.id = sale.id_product " +
                $"{sqlite_query_where_option}" +
                $" GROUP BY sale.id_product";
            try
            {
                using (
                SQLiteConnection connection =
                    new SQLiteConnection(
                        "Data Source=" + Application.StartupPath +
                        "/DB.db;Version=3;New=False;Compress=True;"))
                {
                    connection.Open();

                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    SQLiteDataReader reader = command.ExecuteReader();
                    list = new MySqlFunctions().MapReaderToList<Sale>(reader);

                    reader.Close();
                    connection.Dispose();
                }
            }
            catch (Exception zz)
            {
                System.Diagnostics.Debugger.Break();
            }


            return list;
        }

        public bool Add(BindingList<Product> listNewItem)
        {
            bool response = false;
            string query = "INSERT INTO sale (id_product,quantity,price,datetime) VALUES ";

            for(int i = 0;i < listNewItem.Count;i++)
            {
                query += " ( ";
                query += $"'{listNewItem[i].id}','{1}','{listNewItem[i].cost}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}'";
                    query += $" ) ";

                if (i >= listNewItem.Count - 1)
                {

                }
                else
                    query += $",";
            }


            try
            {
                using (
                SQLiteConnection connection =
                    new SQLiteConnection(
                        "Data Source=" + Application.StartupPath +
                        "/DB.db;Version=3;New=False;Compress=True;"))
                {
                    connection.Open();

                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    response = command.ExecuteNonQuery() == 1 ? true : false;
                    connection.Dispose();

                }
            }
            catch (Exception zz)
            {
                System.Diagnostics.Debugger.Break();
            }


            return response;
        }
    }
}
