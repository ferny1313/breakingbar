﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakingBar.Models
{
    class CustomKeyPress
    {
        /// <summary>
        /// Change KeyPress To Upper, change single quote for null.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static KeyPressEventArgs ChangeToUpperCase(KeyPressEventArgs e, string text_to_compare)
        {
            //IF e == ' IT WILL CHANGE TO EMPTY (\0)
            if (e.KeyChar == '\'')
            {
                e.KeyChar = '\0';
            }

            if (!char.IsControl(e.KeyChar))
            {
                if (char.IsLetterOrDigit(e.KeyChar))
                {
                    e.KeyChar = Char.ToUpper(e.KeyChar);
                }
            }
            return e;
        }


        /// <summary>
        /// Accept only numbers & one dot, this function will not prevent for copy-paste events.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="text_to_compare"></param>
        /// <returns></returns>
        public static System.Windows.Forms.KeyPressEventArgs GetOnlyNumbersAndOneDot(System.Windows.Forms.KeyPressEventArgs e, string text_to_compare)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            if (text_to_compare == "" && e.KeyChar == '.')
                e.KeyChar = '\0';

            //TO ALLOW ONLY ONE DECIMAL POINT           
            if (e.KeyChar == '.' && text_to_compare.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            return e;
        }


        /// <summary>
        /// Get only numbers on KeyPress.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="text_to_compare"></param>
        /// <returns></returns>
        public static KeyPressEventArgs GetOnlyNumbers(KeyPressEventArgs e, string text_to_compare)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            return e;
        }


        public static KeyPressEventArgs WriteNotAllowed(KeyPressEventArgs e, string text_to_compare)
        {
            if (!char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
            return e;
        }





        /// <summary>
        /// If Esc pressed will close the form.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        public static bool PressEsc(ref Message msg, Keys keyData, Form form)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    form.Close();
                    return true;
            }
            return true;
        }

        /// <summary>
        /// If Esc pressed will close the form, if Enter pressed will triger button.click event.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <param name="form"></param>
        /// <param name="button"></param>
        /// <returns></returns>
        public static bool PressEscAndButtonEnterClick(ref Message msg, Keys keyData, Form form, Button button)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    form.Close();
                    return true;
                case Keys.Enter:
                    button.PerformClick();
                    return true;
            }
            return true;
        }

        /// <summary>
        /// If Esc pressed will close the form, if Delete pressed will triger button.click event.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <param name="form"></param>
        /// <param name="button"></param>
        /// <returns></returns>
        public static bool PressEscAndButtonDeleteClick(ref Message msg, Keys keyData, Form form, Button button)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    form.Close();
                    return true;
                case Keys.Delete:
                    button.PerformClick();
                    return true;
            }
            return true;
        }



        /// <summary>
        /// At Esc key, ask for permission and will close given Form.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        public static bool PressEscAskClose(ref Message msg, Keys keyData, Form form)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    if (MessageBox.Show("¿SALIR DEL SISTEMA?", form.Text, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                        form.Close();
                    return true;
            }
            return true;

        }


    }
}
