﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BreakingBar.Models;

namespace BreakingBar.FormControls
{
    public partial class ProductsList : UserControl
    {
        public ProductsList()
        {
            InitializeComponent();
        }

        BindingList<Product> listData;

        private void InventoryList_Load(object sender, EventArgs e)
        {

            listData = new Models.MySqlFunctions().SelectDataFromDB<Product>("");
            dataGridView1.DataSource = listData;
            SetDataGridView(dataGridView1);
        }


        void SetDataGridView(DataGridView dataGridView)
        {
            dataGridView.ReadOnly = true;
            dataGridView.MultiSelect = false;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView.AllowUserToResizeColumns = true;
            dataGridView.AllowUserToOrderColumns = true;
            dataGridView.RowHeadersVisible = false;
            dataGridView.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Regular);
            dataGridView.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);

            //SET COLOR
            dataGridView.DefaultCellStyle.BackColor = Color.LightGreen;
            dataGridView.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView.DefaultCellStyle.SelectionBackColor = Color.GreenYellow;
            dataGridView.DefaultCellStyle.SelectionForeColor = Color.Red;
            dataGridView.ColumnHeadersDefaultCellStyle.BackColor = Color.White;
            dataGridView.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;

            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            dataGridView.GridColor = Color.White;

            dataGridView.Columns[nameof(Product.id)].Visible = false;
            dataGridView.Columns[nameof(Product.cost)].HeaderText = nameof(Product.cost).ToUpper();
            dataGridView.Columns[nameof(Product.name)].HeaderText = nameof(Product.name).ToUpper();
            dataGridView.Columns[nameof(Product.cost)].DefaultCellStyle.Format = "c2";

            dataGridView.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[nameof(Product.cost)].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView.Columns[nameof(Product.name)].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;


            //FORMAT CURRENCY
            System.Globalization.CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Globalization.CultureInfo newCulture = new System.Globalization.CultureInfo(currentCulture.Name);
            newCulture.NumberFormat.CurrencyNegativePattern = 1;

            //dataGridView.EnableHeadersVisualStyles = false;         
            //dataGridView.Columns[nameof(Model.PointOfSale.Quantity_Buying)].DefaultCellStyle.Format = "##00";

            dataGridView.ClearSelection();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {

            {
                Views.ProductAdd formProductAdd = new Views.ProductAdd();
                formProductAdd.ShowDialog();
                if (formProductAdd.Choosen)
                {
                    if( new Product().Add(formProductAdd.newProduct))
                    {

                        Task.Run(() =>
                        {
                            MessageBox.Show("PRODUCT ADDED TO DATABASE", "SUCCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        });
                        RefillListData();

                    }
                }
            }
        }



        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0 && listData.Count > 0)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                Product product = new Product();
                product.id = (int)row.Cells[nameof(Product.id)].Value;
                product.name = (string)row.Cells[nameof(Product.name)].Value;
                product.cost = (float)row.Cells[nameof(Product.cost)].Value;
                Views.ProductEdit formProductAdd = new Views.ProductEdit(product);
                formProductAdd.ShowDialog();
                if (formProductAdd.Choosen)
                {
                    if (new Product().Update(formProductAdd.editedItem))
                    {
                        Task.Run(() =>
                        {
                            MessageBox.Show("PRODUCT UPDATED TO DATABASE", "SUCCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        });
                        RefillListData();
                    }
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0 && listData.Count > 0)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                Product product = new Product();
                product.id = (int)row.Cells[nameof(Product.id)].Value;
                product.name = (string)row.Cells[nameof(Product.name)].Value;
                DialogResult dialogResult = MessageBox.Show($" THE PRODUCT: {product.name} WILL BE DELETED", "WARNING", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                if (dialogResult == DialogResult.OK )
                {
                    if (new Product().Delete(product.id))
                    {
                        Task.Run(() =>
                        {
                            MessageBox.Show("PRODUCT DELETED FROM DATABASE", "DELETION SUCCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        });
                        RefillListData();
                    }
                }
            }
        }

        void RefillListData()
        {
            listData.Clear();
            foreach (Product item in new Models.MySqlFunctions().SelectDataFromDB<Product>(""))
            {
                listData.Add(item);
            }
        }
    }
}
