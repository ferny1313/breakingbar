﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BreakingBar.Models;

namespace BreakingBar.FormControls
{
    public partial class Pos : UserControl
    {
        public Pos()
        {
            InitializeComponent();
        }

        BindingList<Product> listData = new BindingList<Product>();

        private void Pos_Load(object sender, EventArgs e)
        {       
            //REMOVE ALL TEST BUTTONS
            foreach (Button button in flowLayoutPanel1.Controls.OfType<Button>().OrderByDescending(x => x.Text))
            {
                flowLayoutPanel1.Controls.Remove(button);
            }

            //listData = new Models.MySqlFunctions().SelectDataFromDB<Product>("");
            BindingList<Product> listProduct = new Models.MySqlFunctions().SelectDataFromDB<Product>("");

            dataGridView1.DataSource = listData;
            SetDataGridView(dataGridView1);


            foreach (Product item in listProduct)
            {
                Button newButton = new Button();
                newButton.Text = item.name;
                newButton.Tag = item.id.ToString();
                newButton.Click += new EventHandler(this.buttonProduct_Click); ;
                flowLayoutPanel1.Controls.Add(newButton);
            }

           
        }

        void SetDataGridView(DataGridView dataGridView)
        {
            dataGridView.ReadOnly = true;
            dataGridView.MultiSelect = false;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView.AllowUserToResizeColumns = true;
            dataGridView.AllowUserToOrderColumns = true;
            dataGridView.RowHeadersVisible = false;
            dataGridView.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Regular);
            dataGridView.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);

            //SET COLOR
            dataGridView.DefaultCellStyle.BackColor = Color.LightGreen;
            dataGridView.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView.DefaultCellStyle.SelectionBackColor = Color.GreenYellow;
            dataGridView.DefaultCellStyle.SelectionForeColor = Color.Red;
            dataGridView.ColumnHeadersDefaultCellStyle.BackColor = Color.White;
            dataGridView.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;

            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            dataGridView.GridColor = Color.White;

            dataGridView.Columns[nameof(Product.id)].Visible = false;
            dataGridView.Columns[nameof(Product.cost)].HeaderText = nameof(Product.cost).ToUpper();
            dataGridView.Columns[nameof(Product.name)].HeaderText = nameof(Product.name).ToUpper();
            dataGridView.Columns[nameof(Product.cost)].DefaultCellStyle.Format = "c2";

            dataGridView.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[nameof(Product.cost)].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView.Columns[nameof(Product.name)].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;


            //FORMAT CURRENCY
            System.Globalization.CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Globalization.CultureInfo newCulture = new System.Globalization.CultureInfo(currentCulture.Name);
            newCulture.NumberFormat.CurrencyNegativePattern = 1;

            //dataGridView.EnableHeadersVisualStyles = false;         
            //dataGridView.Columns[nameof(Model.PointOfSale.Quantity_Buying)].DefaultCellStyle.Format = "##00";

            dataGridView.ClearSelection();
        }

        private void buttonProduct_Click(object sender, EventArgs e)
        {
          List<Product> listFinded = new Product().GetProduct(Convert.ToInt32((sender as Button).Tag));

            if (listFinded.Count > 0)
            {
                listData.Add(listFinded[0]);
            }

            GetTotalValue();
        }



        public float GetTotalValue()
        {
            float totalValue = 0;
            foreach (var item in listData)
            {
                totalValue += item.cost;
            }
            labelTotal.Text = totalValue.ToString("##.00");

            return totalValue;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count > 0 && listData.Count> 0)
            {
                int id_product = 0;
                DataGridViewRow row = dataGridView1.SelectedRows[0];

                 id_product = (int)row.Cells[nameof(Product.id)].Value;

                foreach(var item in listData)
                {
                    if (item.id == id_product)
                    {
                        listData.Remove(item);
                        GetTotalValue();
                        break;
                    }
                }
            }
        }

        private void buttonPay_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0 && listData.Count > 0)
            {
                float totalCost = GetTotalValue();

                Views.Chargue chargueForm = new Views.Chargue(totalCost);
                chargueForm.ShowDialog();

                if(chargueForm.Choosen)
                {

                    new Sale().Add(listData);

                    MessageBox.Show($"CAMBIO: ${chargueForm.CashChange} \nTRANSACTION PROCESED", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    listData.Clear();
                }                
            }
        }
    }
}
