﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BreakingBar.Models;

namespace BreakingBar.FormControls
{
    public partial class Sales : UserControl
    {
        public Sales()
        {
            InitializeComponent();
        }

        private void Sales_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = new List<Sale>();
            SetDataGridView(dataGridView1);
        }


        void SetDataGridView(DataGridView dataGridView)
        {
            dataGridView.ReadOnly = true;
            dataGridView.MultiSelect = false;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView.AllowUserToResizeColumns = true;
            dataGridView.AllowUserToOrderColumns = true;
            dataGridView.RowHeadersVisible = false;
            dataGridView.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Regular);
            dataGridView.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);

            //SET COLOR
            dataGridView.DefaultCellStyle.BackColor = Color.LightGreen;
            dataGridView.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView.DefaultCellStyle.SelectionBackColor = Color.GreenYellow;
            dataGridView.DefaultCellStyle.SelectionForeColor = Color.Red;
            dataGridView.ColumnHeadersDefaultCellStyle.BackColor = Color.White;
            dataGridView.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;

            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            dataGridView.GridColor = Color.White;

            dataGridView.Columns[nameof(Sale.id_product)].Visible = false;

            dataGridView.Columns[nameof(Sale.product_name)].HeaderText = nameof(Sale.product_name).ToUpper();
            dataGridView.Columns[nameof(Sale.price)].HeaderText = nameof(Sale.price).ToUpper();
            dataGridView.Columns[nameof(Sale.quantity)].HeaderText = nameof(Sale.quantity).ToUpper();

            dataGridView.Columns[nameof(Sale.price)].DefaultCellStyle.Format = "c2";

            dataGridView.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView.Columns[nameof(Sale.product_name)].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView.Columns[nameof(Sale.price)].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;


            //FORMAT CURRENCY
            System.Globalization.CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Globalization.CultureInfo newCulture = new System.Globalization.CultureInfo(currentCulture.Name);
            newCulture.NumberFormat.CurrencyNegativePattern = 1;

            //dataGridView.EnableHeadersVisualStyles = false;         
            //dataGridView.Columns[nameof(Model.PointOfSale.Quantity_Buying)].DefaultCellStyle.Format = "##00";

            dataGridView.ClearSelection();
        }

      

        private void buttonByHour_Click(object sender, EventArgs e)
        {
            string date_time_now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string date_time_last_hour = DateTime.Now.AddHours(-1).ToString("yyyy-MM-dd HH:mm:ss");

            string where_options = $" WHERE strftime('%Y-%m-%d %H:%M:%S', datetime ) " +
                $"                      BETWEEN strftime('%Y-%m-%d %H:%M:%S', '{date_time_last_hour}') " +
                $"                          AND strftime('%Y-%m-%d %H:%M:%S', '{date_time_now}')";
            dataGridView1.DataSource = new Sale().ToList(where_options);
            UpdateDataGrid(new Sale().ToList(where_options));
        }

        private void buttonByDay_Click(object sender, EventArgs e)
        {
            string date_time_now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string date_time_last_day = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");

            string where_options = $" WHERE strftime('%Y-%m-%d %H:%M:%S', datetime ) " +
                $"                      BETWEEN strftime('%Y-%m-%d %H:%M:%S', '{date_time_last_day}') " +
                $"                          AND strftime('%Y-%m-%d %H:%M:%S', '{date_time_now}')";
            dataGridView1.DataSource = new Sale().ToList(where_options);
            UpdateDataGrid(new Sale().ToList(where_options));
        }

        private void buttonByWeek_Click(object sender, EventArgs e)
        {
            string date_time_now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string date_time_last_week = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd HH:mm:ss");

            string where_options = $" WHERE strftime('%Y-%m-%d %H:%M:%S', datetime ) " +
                $"                      BETWEEN strftime('%Y-%m-%d %H:%M:%S', '{date_time_last_week}') " +
                $"                          AND strftime('%Y-%m-%d %H:%M:%S', '{date_time_now}')";
            dataGridView1.DataSource = new Sale().ToList(where_options);
            UpdateDataGrid(new Sale().ToList(where_options));
        }

        private void buttonByMonth_Click(object sender, EventArgs e)
        {
            string date_time_now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string date_time_last_month = DateTime.Now.AddDays(-31).ToString("yyyy-MM-dd HH:mm:ss");

            string where_options = $" WHERE strftime('%Y-%m-%d %H:%M:%S', datetime ) " +
                $"                      BETWEEN strftime('%Y-%m-%d %H:%M:%S', '{date_time_last_month}') " +
                $"                          AND strftime('%Y-%m-%d %H:%M:%S', '{date_time_now}')";
            UpdateDataGrid(new Sale().ToList(where_options));
        }

        void UpdateDataGrid(List<Sale> listData)
        {
            dataGridView1.DataSource = listData;
            float totalSum = 00.00F;
            foreach(Sale item in listData)
            {
                totalSum += item.price * item.quantity;
            }
            textBoxTotal.Text = totalSum.ToString();
        }

        private void dataGridView1_DataSourceChanged(object sender, EventArgs e)
        {

        }
    }
}
