﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakingBar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            buttonPos_Click(sender, e);
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }


        void CloseForms()
        {
            inventoryList1.Hide();
            pos1.Hide();
            sales1.Hide();

            labelTitle.Text = "";
        }

        private void buttoninventory_Click(object sender, EventArgs e)
        {
            CloseForms();
            inventoryList1.Show();
            labelTitle.Text = "INVENTORY";
        }

        private void buttonPos_Click(object sender, EventArgs e)
        {
            CloseForms();
            pos1.Show();
            labelTitle.Text = "POS";

        }

        private void buttonSales_Click(object sender, EventArgs e)
        {
            CloseForms();
            sales1.Show();
            labelTitle.Text = "SALES";
        }




        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Escape))
            {
                // Alt+F pressed
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pos1_Load(object sender, EventArgs e)
        {

        }
    }
}
